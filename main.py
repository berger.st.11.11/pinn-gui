from tkinter import *
from tkinter import font
import tkinter 
import customtkinter

from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
import numpy as np  

import matplotlib.animation as animation
from matplotlib.widgets import Slider, Button
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib import gridspec
from scipy.interpolate import *
import numpy as np
import torch
import natsort 
import meshio

import webbrowser  
import pickle

import os, sys
import time
import PIL

# Set paths to known folder structure for different PINNs and lib files.
########################################################################################
file_path = os.path.dirname(__file__)
# Absolute path to lib folder
parent_dir = '/'.join([file_path,'..'])
sys.path.append(parent_dir)



from libs.utils.flowField import FlowField
from libs.plot.export_non_stationary_PINN_result import export_non_stationary_PINN_result



customtkinter.set_appearance_mode("System")  # Modes: system (default), light, dark
customtkinter.set_default_color_theme("blue")  # Themes: blue (default), dark-blue, green
# plt.style.use('dark_background')
plt.style.use('grayscale')

# the main Tkinter window
root = customtkinter.CTk()
  
# setting the title 
root.title('PINN Visualization')

# root.config(bg="white")  

# dimensions of the main window
root.geometry("1900x1100")

# Creating a Font object of "TkDefaultFont"
defaultFont = font.nametofont("TkDefaultFont")

# Overriding default-font with custom settings
# i.e changing font-family, size and weight
defaultFont.configure(family="DejaVu Sans", size=10, weight=font.NORMAL)


  

### DEFINE GUI LAYOUT

spline_editor_frame = customtkinter.CTkFrame(root, width=1380, height=480)
spline_editor_frame.pack(anchor=N, padx=10, pady=10)

spline_editor_settings_frame = customtkinter.CTkFrame(spline_editor_frame, width=180, height=200)
spline_editor_settings_frame.pack(side=LEFT, padx=10, pady=10, fill='y')

# spline_editor_settings_list_frame = customtkinter.CTkFrame(spline_editor_settings_frame, width=180, height=200)
# spline_editor_settings_frame.pack(padx=10, pady=10)

spline_editor_plot = customtkinter.CTkFrame(spline_editor_frame, width=1000, height=280)
spline_editor_plot.pack(side=RIGHT, padx=10, pady=10)

result_frame = customtkinter.CTkFrame(root, width=1380, height=680)
result_frame.pack(anchor=S, padx=10, pady=10)

result_settings_frame = customtkinter.CTkFrame(result_frame, width=180, height=460, )
result_settings_frame.pack(padx=10, pady=10, anchor=W, expand=False)

result_display_frame = customtkinter.CTkFrame(result_frame, width=1200, height=460,)
result_display_frame.pack(padx=10, pady=10, anchor=E, expand=False)



#####################
### SPLINE EDITOR ###
#####################

px = 1/plt.rcParams['figure.dpi']  # pixel in inches

T_START = 300
T_MIN   = 250
T_MAX   = 350

#get a list of points to fit a spline to as well
N = 5
xmin = 0 
xmax = 2.5
x = np.linspace(xmin,xmax,N)

#spline fit
xvals = x
yvals = T_START * np.ones(x.shape)
spline = interp1d(xvals, yvals, kind="linear", bounds_error=False)

#figure.subplot.right

#set up a plot
fig_spline_editor = Figure(figsize=(1000*px, 280*px), dpi=100)
ax_spline_editor = fig_spline_editor.add_subplot()


pind = None #active point
epsilon = 5 #max pixel distance


X = np.arange(0,xmax+1,0.01)
l, = ax_spline_editor.plot (xvals,yvals,color='b',linestyle='none',marker='o',markersize=8, zorder=10)
m, = ax_spline_editor.plot (X, spline(X), 'r-')


def update(val):
    global yvals
    global xvals
    global spline
    # update curve

    l.set_ydata(yvals)
    l.set_xdata(xvals)
    spline =interp1d(xvals, yvals, kind="linear",bounds_error=False)
    m.set_ydata(spline(X))
    # redraw canvas while idle
    fig_spline_editor.canvas.draw_idle()

def reset(event):
    global yvals
    global spline
    #reset the values
    xvals = x
    yvals = func(x)

    spline =  interp1d(x, yvals, kind='linear',bounds_error=False)
    l.set_ydata(yvals)
    m.set_ydata(spline(X))
    # redraw canvas while idle
    fig_spline_editor.canvas.draw_idle()

def button_press_callback(event):
    'whenever a mouse button is pressed'
    global pind
    if event.inaxes is None:
        return
    if event.button != 1:
        return
    pind = get_ind_under_point(event) 

def button_release_callback(event):
    'whenever a mouse button is released'
    global pind
    if event.button != 1:
        return
    pind = None
    canvas_spline_editor.draw()

def get_ind_under_point(event):
    'et the index of the vertex under point if within epsilon tolerance'

    # display coords
    #print('display x is: {0}; display y is: {1}'.format(event.x,event.y))
    t = ax_spline_editor.transData.inverted()
    tinv = ax_spline_editor.transData 
    xy = t.transform([event.x,event.y])
    #print('data x is: {0}; data y is: {1}'.format(xy[0],xy[1]))
    xr = np.reshape(x,(np.shape(x)[0],1))
    yr = np.reshape(yvals,(np.shape(yvals)[0],1))
    xy_vals = np.append(xr,yr,1)
    xyt = tinv.transform(xy_vals)
    xt, yt = xyt[:, 0], xyt[:, 1]
    d = np.hypot(xt - event.x, yt - event.y)
    indseq, = np.nonzero(d == d.min())
    ind = indseq[0]

    #print(d[ind])
    if d[ind] >= epsilon:
        ind = None
    
    #print(ind)
    return ind

def motion_notify_callback(event):
    'on mouse movement'
    global yvals
    if pind is None:
        return
    if event.inaxes is None:
        return
    if event.button != 1:
        return
    
    #update yvals
    # print('motion x: {0}; y: {1}'.format(event.xdata,event.ydata))
    yvals[pind] = event.ydata 
    # xvals[pind] = event.xdata

    sliders[pind].set_val(yvals[pind])
    # update curve via sliders and draw
    fig_spline_editor.canvas.draw_idle()
    canvas_spline_editor.draw()


ax_spline_editor.set_yscale('linear')
ax_spline_editor.set_xlim(-0.1, xmax+0.1)
ax_spline_editor.set_ylim(T_MIN,T_MAX)
ax_spline_editor.set_xlabel('Time [s]')
ax_spline_editor.set_ylabel('Temperature [K]')
ax_spline_editor.set_title('Temperature of the Cylinder')
ax_spline_editor.grid(True)
ax_spline_editor.yaxis.grid(True,which='minor',linestyle='--')

sliders = []

for i in np.arange(N):

    axamp = plt.axes([0.84, 0.8-(i*0.05), 0.12, 0.02])
    # Slider
    s = Slider(axamp, 'p{0}'.format(i), 0, 10, valinit=yvals[i])
    sliders.append(s)

    
for i in np.arange(N):
    #samp.on_changed(update_slider)
    sliders[i].on_changed(update)

axres = plt.axes([0.84, 0.8-((N)*0.05), 0.12, 0.02])
bres = Button(axres, 'Reset')
bres.on_clicked(reset)


canvas_spline_editor = FigureCanvasTkAgg(fig_spline_editor, master=spline_editor_plot)  # A tk.DrawingArea.

fig_spline_editor.canvas.mpl_connect('button_press_event', button_press_callback)
fig_spline_editor.canvas.mpl_connect('button_release_event', button_release_callback)
fig_spline_editor.canvas.mpl_connect('motion_notify_event', motion_notify_callback)

canvas_spline_editor.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=True)



################################



def refresh_timeline(val):

    val = float(val)
    index = int(100.0 * val)

    # Clear pervious plot
    ax_display.clear()
    # Load data
    x, y, T, t_plot, U, V, X, Y = plots[index]
    # Add contour plot
    result_contour = ax_display.tricontourf(x,y,T, 100, cmap= 'seismic', vmin=260, vmax=340)
    # Add Streamlines
    speed = np.sqrt(U**2 + V**2)
    lw = speed / speed.max()
    # xlist = np.linspace(0, 1.2, 30)
    # ylist = np.linspace(0, 0.41, 20)
    # X, Y = np.meshgrid(xlist, ylist)    
    result_streamlines = ax_display.streamplot(X, Y, U ,V ,linewidth=lw, color='gray')
    # Add cylinder
    circle = plt.Circle((0.2, 0.2), radius=0.05, color='grey', zorder=10)
    ax_display.add_patch(circle)

    ax_display.set_xlim(0.0, 1.2)
    ax_display.set_ylim(0.0, 0.41)

    fig_display.canvas.draw_idle()



def refresh_button():
    global pinn

    # Specify export parameter
    pinn_config_path        = "/".join([file_path, "config_pinn.yml"])
    mesh_file_plot_path     = "/".join([file_path, "cylinder_flow_very_fine.msh"])
    param_config_file_path  = "/".join([file_path, "config_parameterization.yml"])
    parameters              = yvals # get the values from the spline editor
    export_path_vtu             = "/".join([file_path, "pinn_results/"])
    export_name             = "cylinder_flow"
    t_delta_plot            = 0.01

    pinn = torch.load("/".join([file_path, "ParameterizedCylinder5PointsWithFlow"]))
    pinn.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # Export pinn
    export_non_stationary_PINN_result(pinn_config_path, mesh_file_plot_path, param_config_file_path, pinn,
                                    parameters, export_path_vtu, export_name, t_delta_plot)
    load_and_display_vtu_files()

    refresh_timeline(timeline_slider_value.get())


def play_button():

    for i in range(50):
        timeline_slider_value.set(i*0.05)

        refresh_timeline(timeline_slider_value.get())

        root.update_idletasks()
        root.update()


    

def load_and_display_vtu_files():
    global plots
    plots = []

    export_folder = "/".join([file_path, "pinn_results/"])
    t_plot = 0.0

    for i, file in enumerate(natsort.natsorted(os.listdir(export_folder))):
        if file.endswith(".vtu"):
            print(file)

            mesh = meshio.read("/".join([export_folder, file]))
            # Pre-compute velocity field
            xlist = np.linspace(0, 1.2, 20)
            ylist = np.linspace(0, 0.41, 10)
            X, Y = np.meshgrid(xlist, ylist)    
            velocity = flow_field.eval(t_plot, X, Y ,0.0)
            U = velocity[:,:,0]
            V = velocity[:,:,1]

            contour_plot_data = [mesh.points[:,0],mesh.points[:,1], mesh.point_data["Temperature"],t_plot, U, V, X, Y]
            plots.append(contour_plot_data)
            t_plot += 0.01


# Get flow field
flow_field = pickle.load(open('/'.join([file_path, "flow_field.p"]), "rb"))

# set up a plot
# fig_display = Figure(figsize=(1200*px, 480*px), dpi=100)
fig_display, (ax_display, ax_colorbar) = plt.subplots(1,2, figsize=(1900*px, 680*px), gridspec_kw={'width_ratios': [15, 1]})

fig_display.tight_layout()
# gs = gridspec.GridSpec(1, 2, width_ratios=[3, 1]) 
# ax_display = fig_display.add_subplot(1,2,1, gridspec_kw={'width_ratios': [3, 1]})
# ax_colorbar = fig_display.add_subplot(1,2,2, gridspec_kw={'width_ratios': [3, 1]})


result_contour = ax_display.plot()

ax_display.set_xlim(0.0, 1.2)
ax_display.set_ylim(0.0, 0.41)

cb = mpl.colorbar.ColorbarBase(ax_colorbar, orientation='vertical', 
                               cmap=mpl.cm.seismic,
                               norm=mpl.colors.Normalize(250, 350)),  # vmax and vmin

ax_colorbar.tick_params(rotation=-90)


canvas_display = FigureCanvasTkAgg(fig_display, master=result_display_frame)  # A tk.DrawingArea.
canvas_display.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=True)




# Set up control elements

# Spacer such that time axis and spline editor line up
spacer = tkinter.Label(result_settings_frame, text="",background='#333333',highlightbackground = "#333333", highlightcolor= "#333333").pack(side=LEFT, padx=190)

refresh_icon = customtkinter.CTkImage(PIL.Image.open("/".join([file_path, r"graphicElements\refresh.png"])))
refresh_button = customtkinter.CTkButton(result_settings_frame, image=refresh_icon, command= refresh_button,text="" ).pack(side = LEFT,padx=5)

play_icon = customtkinter.CTkImage(PIL.Image.open("/".join([file_path, r"graphicElements\play.png"])))
play_button = customtkinter.CTkButton(result_settings_frame, image=play_icon, command= play_button,text="" ).pack(side = LEFT,padx=5 )

timeline_slider_value = StringVar()
timeline_slider = tkinter.Scale(result_settings_frame, from_=0, to=2.5,resolution=0.01,variable=timeline_slider_value, background='#333333', fg='grey', highlightbackground = "#333333", highlightcolor= "#333333",
                                 orient=HORIZONTAL, length=900, tickinterval=0.1, label="Time [s]", command=refresh_timeline).pack(side=RIGHT, fill='x',padx=40)

# ADD CATS LOGO

def link_to_home_page():
    webbrowser.open("https://www.cats.rwth-aachen.de/go/id/nohs/", new=0, autoraise=True)


logo_image = customtkinter.CTkImage(PIL.Image.open("/".join([file_path, r"graphicElements\cats_logo.png"])),  size=(260,60))
logo_image_button = customtkinter.CTkButton(spline_editor_settings_frame, image=logo_image, command= link_to_home_page, text="", fg_color='#333333').pack(anchor='n', pady=15, padx=5)


listNodes = Listbox(spline_editor_settings_frame, width=30, height=5, font=("Helvetica", 12), background="grey")
listNodes.pack(side="left" , fill="y")

scrollbar = Scrollbar(spline_editor_settings_frame, orient="vertical")
scrollbar.config(command=listNodes.yview)
scrollbar.pack(side="right", fill="y")

listNodes.config(yscrollcommand=scrollbar.set)

listNodes.insert(END, " Cylinder")
listNodes.insert(END, " Inflow")
listNodes.insert(END, " Outflow")
listNodes.insert(END, " Top Wall")
listNodes.insert(END, " Bottom Wall")


# run the gui
root.mainloop()